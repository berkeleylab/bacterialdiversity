### Supporting data for  

## Towards a balanced view of the bacterial tree of life    
  
*Frederik Schulz, Emiley A. Eloe-Fadrosh, Robert Bowers, Jessica Jarett, Torben Nielsen, Natalia N. Ivanova, Nikos C. Kyrpides, Tanja Woyke*

This repository contains the phylogenetic tree described in the paper and the underlying sequence alignment. Please contact Frederik Schulz (fschulz@lbl.gov) for any questions regarding the data and their use.

### Terminology

* Accession numbers of SSU rRNA sequences used in this study are provided in sequences/allSSU.lookup

   * IMGGBAC -> bacterial SSU rRNA sequences extracted from genomes in IMG
   * MTGBAC ->  bacterial SSU rRNA sequences extracted from metagenomes in IMG
   * OUT -> archaeal SSU rRNA sequences used as outgroup in the bacterial tree